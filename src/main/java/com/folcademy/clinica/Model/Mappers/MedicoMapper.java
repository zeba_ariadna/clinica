package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {

    private final PersonaMapper personaMapper;

    public MedicoMapper(PersonaMapper personaMapper) {
        this.personaMapper = personaMapper;
    }


    public MedicoEnteroDto entityToEnteroDto(Medico entity){
       return Optional
               .ofNullable(entity)
               .map(
                       ent -> new MedicoEnteroDto(
                               ent.getId(),
                               ent.getProfesion(),
                               ent.getConsulta(),
                               ent.getPersona()
                       )
               )
               .orElse(new MedicoEnteroDto());
   }

   public Medico enteroDtoToEntity(MedicoEnteroDto dto, Persona persona){
       Medico entity= new Medico();
       entity.setId(dto.getId());
       entity.setProfesion(dto.getProfesion());
       entity.setConsulta(dto.getConsulta());
       entity.setPersona(persona);
       entity.setIdpersona(dto.getPersona().getIdpersona());

       return entity;
   }
}
