package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    private final PersonaMapper personaMapper;

    public PacienteMapper(PersonaMapper personaMapper) {
        this.personaMapper = personaMapper;
    }


    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getIdpaciente(),
                                ent.getDireccion(),
                                ent.getPersona()
                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto, Persona persona){
        Paciente entity = new Paciente();
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setDireccion(dto.getDireccion());
        entity.setPersona(persona);
        entity.setIdpersona(dto.getPersona().getIdpersona());

        return entity;
    }

}
