package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "persona")
@Getter
@Setter
@RequiredArgsConstructor
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpersona",columnDefinition = "VARCHAR")
    public Integer idpersona;

    @Column(name = "dni", columnDefinition = "VARCHAR")
    public String dni="";

    @Column(name = "nombre", columnDefinition = "VARCHAR")
    public String nombre = "";

    @Column(name = "apellido", columnDefinition = "VARCHAR")
    public String apellido = "";

    @Column(name = "telefono", columnDefinition = "VARCHAR")
    public String telefono ="";

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if(o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Persona persona= (Persona) o;

        return Objects.equals(idpersona, persona.idpersona);
    }

    @Override
    public int hashCode(){
        return getClass().hashCode();
    }
}
