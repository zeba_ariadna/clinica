package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "paciente")
@Getter
@Setter
@RequiredArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    Integer idpaciente;

    @Column(name = "direccion", columnDefinition = "VARCHAR")
    public String direccion ="";

    @Column(name = "idpersona",columnDefinition = "INT")
    public Integer idpersona;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpersona",referencedColumnName = "idpersona", insertable = false,updatable = false)
    private Persona persona;

    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Paciente paciente = (Paciente) o;

        return Objects.equals(idpaciente, paciente.idpaciente);
    }

    @Override
    public int hashCode(){
        return getClass().hashCode();
    }

}
