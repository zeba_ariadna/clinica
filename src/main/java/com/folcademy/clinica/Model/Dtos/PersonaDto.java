package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDto {
    public Integer idpersona;
    public String dni = "";
    public String nombre = "";
    public String apellido = "";
    public String telefono = "";

}
