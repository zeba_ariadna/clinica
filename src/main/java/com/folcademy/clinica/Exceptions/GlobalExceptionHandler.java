package com.folcademy.clinica.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler (RuntimeException.class)
    @ResponseBody
    public ResponseEntity<ErrorMenssage> defaultErrorHandler(HttpServletRequest req, Exception e) {
        return new ResponseEntity<ErrorMenssage>(new ErrorMenssage("Error Generico", e.getMessage(), e.toString(), req.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorMenssage> notFoundHandler(HttpServletRequest req,Exception e){//cuando queremos listar
        return  new ResponseEntity<ErrorMenssage>(new ErrorMenssage("Not found", e.getMessage(),"2",req.getRequestURI()),HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseEntity<ErrorMenssage> badRequestException(HttpServletRequest req,Exception e){
        return  new ResponseEntity<ErrorMenssage>(new ErrorMenssage("Bad request", e.getMessage(),"3",req.getRequestURI()),HttpStatus.BAD_REQUEST);
    }


}
