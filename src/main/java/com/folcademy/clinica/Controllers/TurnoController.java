package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/turnos")
public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @GetMapping("/page")
    public ResponseEntity<Page<TurnoDto>> listarTodosByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "idturno") String orderField
    ){
        return ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber,pageSize,orderField)); }

    @GetMapping("/{idturno}")
    public  ResponseEntity <Page<TurnoDto>>listarUno(
            @PathVariable(name="idturno")int id,
            @RequestParam(name = "pageNumber",defaultValue ="0")Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue ="1")Integer pageSize){
        return  ResponseEntity.ok(turnoService.listaUnoByPage(id,pageNumber,pageSize));
    }

    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody TurnoDto dto){
        return ResponseEntity.ok(turnoService.agregar(dto));
    }

    @PutMapping("/{idturno}")
    public ResponseEntity<TurnoDto> editar(@PathVariable (name="idturno") int id, @RequestBody TurnoDto dto){
        return ResponseEntity.ok(turnoService.editar(id,dto));
    }

    @DeleteMapping("/{idturno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idturno") int idturno){
        return ResponseEntity.ok(turnoService.eliminar(idturno));
    }


}
