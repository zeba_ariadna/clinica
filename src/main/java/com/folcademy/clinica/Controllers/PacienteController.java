package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }


    @GetMapping("/page")
    public ResponseEntity<Page<PacienteDto>> listarTodosByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "idpersona") String orderField
    ){
        return ResponseEntity.ok(pacienteService.listarTodosByPage(pageNumber,pageSize,orderField));
    }

    @GetMapping("/{idpaciente}")
    public  ResponseEntity<Page<PacienteDto>>listarUno(
            @PathVariable(name="idpaciente")int id,
            @RequestParam(name = "pageNumber",defaultValue ="0")Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue ="1")Integer pageSize)
    {
        return  ResponseEntity.ok(pacienteService.listarUnoByPage(id,pageNumber,pageSize));
    }

    @PostMapping("")
    public ResponseEntity<PacienteDto> agregar(@RequestBody PacienteDto dto){
        return ResponseEntity.ok(pacienteService.agregar(dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PacienteDto> editar(@PathVariable(name = "id") int id, @RequestBody PacienteDto dto){
        return ResponseEntity.ok(pacienteService.editar(id,dto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id){
        return ResponseEntity.ok(pacienteService.eliminar(id));

    }

}
