package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService){
        this.medicoService =medicoService;
    }


    @GetMapping("/page")
    public ResponseEntity<Page<MedicoEnteroDto>>listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize
            //@RequestParam(name = "orderField", defaultValue = "apellido") String orderField

    ) {
        return ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber, pageSize));
    }

    @GetMapping("/{idMedico}")
    public  ResponseEntity<Page<MedicoEnteroDto>>listarUno(
            @PathVariable(name="idMedico")int id,
            @RequestParam(name = "pageNumber",defaultValue ="0")Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue ="1")Integer pageSize)
    {
        return  ResponseEntity.ok(medicoService.listaUnoByPage(id,pageNumber,pageSize));
    }

    @PostMapping("")
    public ResponseEntity<MedicoEnteroDto> agregar(@RequestBody MedicoEnteroDto dto){
        return ResponseEntity.ok(medicoService.agregar(dto));
    }

    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar (@PathVariable(name = "idMedico") int id, @RequestBody MedicoEnteroDto dto){
        return ResponseEntity.ok(medicoService.editar(id,dto));
    }

    @PutMapping("/{idMedico}/consulta/{consulta}")
    public ResponseEntity<Boolean> editarConsulta(@PathVariable(name = "idMedico") int id,@PathVariable(name = "consulta") int consulta){
        return ResponseEntity.ok(medicoService.editarConsulta(id,consulta));
    }

    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") int id){
        return ResponseEntity.ok(medicoService.eliminar(id));
    }


}
