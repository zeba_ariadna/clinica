package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("turnoService")
public class TurnoService implements ITurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;


    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }

    public TurnoDto agregar(TurnoDto entity){
        entity.setIdturno(null);
        /*if(entity.getFecha().equals(null)){
            throw new BadRequestException("La fecha no debe estar vacía");
        } else{
            if(entity.getHora().equals(null)){
                throw new BadRequestException("La hora no debe estar vacía");
            } else{
                if(entity.getAtendido() == null){
                    throw new BadRequestException("Debe haber true o false");
                } else {
                    if(entity.getIdpaciente() == null){
                        throw new BadRequestException("Idpaciente no puede estar vacío");
                    } else {
                        if(entity.getIdmedico()==null){
                            throw new BadRequestException("Idmedico no puede estar vacío");
                        }
                    }
                }
            }
        }*/
        return turnoMapper
                .entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity)));
    }

    public TurnoDto editar(Integer idturno, TurnoDto dto){
        if(!turnoRepository.existsById(idturno))
            throw new NotFoundException("No existe un turno con id: "+idturno);
        dto.setIdturno(idturno);
        return turnoMapper.entityToDto(
                turnoRepository.save(turnoMapper.dtoToEntity(dto))
        );
    }

    public boolean eliminar(Integer idturno){
        if(!turnoRepository.existsById(idturno))
            throw new NotFoundException("No existe turno con id: "+idturno);
        turnoRepository.deleteById(idturno);
            throw new NotFoundException("Turno eliminado");
    }

    public Page<TurnoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }

    public Page<TurnoDto> listaUnoByPage(int id ,Integer pageNumber, Integer pageSize) {
        Pageable pageable= PageRequest.of(pageNumber, pageSize);
        return turnoRepository.findByIdturno(id,pageable).map(turnoMapper::entityToDto);
    }

}
