package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaMapper personaMapper;
    private final PersonaRepository personaRepository;


    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaMapper personaMapper, PersonaRepository personaRepository)
    {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaMapper = personaMapper;
        this.personaRepository = personaRepository;
    }

    public PacienteDto agregar(PacienteDto dto){//Revisar
        dto.setIdpaciente(null);
        Persona persona = dto.getPersona();
        personaRepository.save(persona);
        return pacienteMapper
                .entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(dto,persona)));

    }

    public PacienteDto editar(Integer id, PacienteDto dto){//Revisar
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("No existe paciente con id: "+id);
        //return null;

        if(personaRepository.existsById(dto.getPersona().getIdpersona()))
            if(pacienteRepository.existsById(id))
                dto.setIdpaciente(id);

        Persona persona= dto.getPersona();

        return pacienteMapper.entityToDto(
                pacienteRepository.save(
                        pacienteMapper.dtoToEntity(dto,persona)
                )
        );

    }

    public boolean eliminar(Integer id){
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Paciente no encontrado, id: "+id);
        //return false;
        pacienteRepository.deleteById(id);
            throw new NotFoundException("Paciente eliminado");
        //return true;
    }

    public Page<PacienteDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }

    public Page<PacienteDto> listarUnoByPage(int id ,Integer pageNumber, Integer pageSize) {
        Pageable pageable= PageRequest.of(pageNumber, pageSize);
        return pacienteRepository.findByIdpaciente(id,pageable).map(pacienteMapper::entityToDto);
    }
}
