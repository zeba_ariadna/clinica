package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final PersonaMapper personaMapper;
    private final PersonaRepository personaRepository;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper, PersonaMapper personaMapper,PersonaRepository personaRepository) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaMapper = personaMapper;
        this.personaRepository = personaRepository;
    }


    public MedicoEnteroDto agregar(MedicoEnteroDto dto){
        dto.setId(null);

        Persona persona = dto.getPersona();
        personaRepository.save(persona);

        if(dto.getConsulta()<0){
            throw new BadRequestException("La consulta no puede ser menor a cero");
        }
        if(persona.getIdpersona()!=null)
            return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(dto,persona)));
        else throw new BadRequestException("Persona vacía");

    }

    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto){//Revisar
        if(!medicoRepository.existsById(idMedico))
            throw new NotFoundException("No existe médico con id: "+idMedico);
        if (dto.getConsulta()<0)
            throw new BadRequestException("La consulta no puede ser menor a 0");
        if (dto.getProfesion()=="")
            throw new BadRequestException("La profesión no debe estar vacía");
        if(personaRepository.existsById(dto.getPersona().getIdpersona()))
            if(medicoRepository.existsById(idMedico))
                dto.setId(idMedico);

        Persona persona = dto.getPersona();

        return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(dto,persona)));
    }

    public boolean editarConsulta(Integer idMedico,Integer consulta){
        if(medicoRepository.existsById(idMedico)){
            Medico entity = medicoRepository.findById(idMedico)
                    .orElse(new Medico());
            entity.setConsulta(consulta);
            medicoRepository.save(entity);
            return true;
        }
        else
          throw new NotFoundException("No se puede editar la consulta, porque no existe médico con id: "+idMedico);
          //return false;
    }

    public boolean eliminar(Integer id) {
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("No hay médico con id: "+id);
            //return false;
        medicoRepository.deleteById(id);
            throw new NotFoundException("Médico eliminado");
            //return true;
    }

    public Page<MedicoEnteroDto> listarTodosByPage(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize);
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToEnteroDto);
    }

    public Page<MedicoEnteroDto> listaUnoByPage(int id ,Integer pageNumber, Integer pageSize) {
        Pageable pageable= PageRequest.of(pageNumber, pageSize);
        return medicoRepository.findById(id,pageable).map(medicoMapper::entityToEnteroDto);
    }
}
